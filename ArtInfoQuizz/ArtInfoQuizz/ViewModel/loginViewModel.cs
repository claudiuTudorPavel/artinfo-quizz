﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using ArtInfoQuizz.Model;

namespace ArtInfoQuizz.ViewModel
{
	public class loginViewModel
	{
		public MyICommand DeleteCommand { get; set; }

		public loginViewModel()
		{
			LoadLogs();
			DeleteCommand = new MyICommand(OnDelete, CanDelete);
		}

		public ObservableCollection<login> LogIns
		{
			get; set;
		}

		public void LoadLogs()
		{
			ObservableCollection<login> logs = new ObservableCollection<login>();
			
			logs.Add(new login { User = "Bau", Password = "BauBau" });
			logs.Add(new login { User = "Clau", Password = "miau" });
			logs.Add(new login { User = "Pao", Password = "miaumaiu" });
			logs.Add(new login { User = "Hau", Password = "bau" });
			LogIns = logs;

		}


		private login _selectedLog;

		public login SelectedLog
		{
			get
			{
				return _selectedLog;
			}

			set
			{
				_selectedLog = value;
				DeleteCommand.RaiseCanExecuteChanged();
			}
		}

		private void OnDelete()
		{
			login.Remove(SelectedLog);
		}

		private bool CanDelete()
		{
			return SelectedLog != null;
		}

	}

}
