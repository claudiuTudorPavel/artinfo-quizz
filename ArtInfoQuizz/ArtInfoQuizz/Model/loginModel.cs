﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ArtInfoQuizz.Model
{
	public class loginModel
	{
	}


	public class login: INotifyPropertyChanged
	{
		private string user;
		private string password;

		public string User
		{
			get { return user; }
			set
			{
				if(user!=value)
				{
					user = value;
					RaisePropertyChanged("User");
					RaisePropertyChanged("userAndPass");
				}
			}
		}

		public string Password
		{
			get { return password; }
			set
			{
				if(password!=value)
				{
					password = value;
					RaisePropertyChanged("Password");
					RaisePropertyChanged("userAndPass");
				}
			}
		}

		public string userAndPass
		{
			get { return user + " " + password; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void RaisePropertyChanged(string property)
		{
			if(PropertyChanged!=null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}

		
	}






}
