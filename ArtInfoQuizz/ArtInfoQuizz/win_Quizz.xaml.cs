﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ArtInfoQuizz
{
	/// <summary>
	/// Interaction logic for win_Quizz.xaml
	/// </summary>
	public partial class win_Quizz : Window
	{
		public int i = 0;
		public int j = 1;
		//generam 5 numere aleatorii de la 1 la 10
		public int[] questions = randomArray();
		public XmlDocument xmlDocument = generateXML();
		public RadioButton[] radioButtons;
		public int score = 0;
		public int finalScore;
		public int truePos;
		public static String Alt;



		public win_Quizz(String difficulty)
		{
			Alt = difficulty;
			InitializeComponent();
			
			generateQuestion(xmlDocument);
			//generateButtons(xmlDocument);
			radioButtons = generateButtons(xmlDocument);
			addRadioButtonsToGrid(radioButtons);

		}
		

		

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			
			if(areCheckedRadioButtons(radioButtons)==false)
			{
				MessageBox.Show("Please choose one answer.");
			}
			else
			{
				finalScore = trackScore(radioButtons);
				lblCurrentScore.Content = finalScore;
				//lblCurrentScore.Content = finalScore;

				i++;

				myGrid.Children.Clear();

				if (i == questions.Length)
				{
					loadResults();
				}
				else
				{
					generateQuestion(xmlDocument);
					//generateButtons(xmlDocument);
					radioButtons = generateButtons(xmlDocument);
					addRadioButtonsToGrid(radioButtons);
					

				}
			}

				
		}

		public static XmlDocument generateXML()
		{
			XmlDocument xmlDoc = new XmlDocument();
			
			xmlDoc.Load("D:\\Dropbox\\ArtInfo\\Quiz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\QuizzQuestions.xml");

			return xmlDoc;
		}

		public void generateQuestion(XmlDocument xmlDoc)
		{
			XmlNode titleNode = xmlDoc.SelectSingleNode("//quizz/" + Alt +"/question[@number='" + questions[i] + "']/text");
			lblQuestion.Content = titleNode.InnerText;
		}

		//public void generateButtons(XmlDocument xmlDoc)
		//{
		//	//calculam cate optiuni are intrebarea
		//	int optionsNumber = (int)numberOfOptions(questions, i);

		//	//creez array cu numar necesar de butoane 
		//	RadioButton[] radioButtons = new RadioButton[optionsNumber];

		//	//iterez prin array si creez butoane pentru fiecare pozitie
		//	for (int pos = 0; pos < optionsNumber; pos++)
		//	{
		//		XmlNode tempOptionNode = xmlDoc.SelectSingleNode("//quizz/medium/question[@number='" + questions[i] + "']/option[@number='" + j + "']");
		//		radioButtons[pos] = new RadioButton();
		//		radioButtons[pos].Content = tempOptionNode.InnerText;
		//		radioButtons[pos].Margin = new Thickness(15, 35 + pos * 25, 0, 0);
		//		myGrid.Children.Add(radioButtons[pos]);
		//		j++;
		//	}

		//	j = 1;

		//}

		public RadioButton[] generateButtons(XmlDocument xmlDoc)
		{
			//calculam cate optiuni are intrebarea
			int optionsNumber = (int)numberOfOptions(questions, i);
			//MessageBox.Show("Number of options counted: " + optionsNumber);

			//creez array cu numar necesar de butoane 
			RadioButton[] radioButtons = new RadioButton[optionsNumber];

			//iterez prin array si creez butoane pentru fiecare pozitie
			for (int pos = 0; pos < optionsNumber; pos++)
			{
				XmlNode tempOptionNode = xmlDoc.SelectSingleNode("//quizz/" + Alt + "/question[@number='" + questions[i] + "']/option[@number='" + j + "']");
				radioButtons[pos] = new RadioButton();
				//MessageBox.Show("tempOption at at i= " + i + " q[i]= " + questions[i] + " j: " + j);
				radioButtons[pos].Content = tempOptionNode.InnerText;
				radioButtons[pos].Margin = new Thickness(15, 35 + pos * 25, 0, 0);
				//lblJayIndex.Content = "Jay Index: " + j;
				//lblAiIndex.Content = "I Index: " + questions[i];

				if (tempOptionNode.Attributes["correct"].Value == "true")
				{
					truePos = j;
					
				}
				
				j++;
			}

			j = 1;

			return radioButtons;
		}

		

		

		public void addRadioButtonsToGrid(RadioButton[] radioButtons)
		{
			for(int d=0; d<radioButtons.Length; d++)
			{
				myGrid.Children.Add(radioButtons[d]);
			}
		}

		public Boolean areCheckedRadioButtons(RadioButton[] radioButtons)
		{
			Boolean checkedButtons = false;

			for (int s = 0; s < radioButtons.Length; s++)
			{
				if (radioButtons[s].IsChecked == true)
				{
					return true;
				}
			}

			return checkedButtons;
		}

		public int trackScore(RadioButton[] radioButtons)
		{
			for (int s = 0; s < radioButtons.Length; s++)
			{
				if (radioButtons[s].IsChecked == true) 
				{
					if(s+1 == truePos)
					{
						score++;
					}				

					
				}
			}

					return score;
		}

		public static int[] randomArray()
		{
			List<int> futureArray = randomListGenerator();
			int[] arr = new int[futureArray.Count];
			
			for(int k=0;k<arr.Length;k++)
			{
				arr[k] = futureArray[k];
				
			}

			return arr;
		}

		public static List<int> randomListGenerator()
		{
			List<int> randomList = new List<int>();
			Random generator = new Random();
			int myNumber = generator.Next(1, 10);
			int h = 0;

			while (h<5)
			{
				if(!randomList.Contains(myNumber))
				{
					randomList.Add(myNumber);
					h++;
				}
				myNumber = generator.Next(1, 10);
				
				
			}

			return randomList;
		}

		public static double numberOfOptions(int[] questions, int i)
		{
			
			var xDoc = XDocument.Load("D:\\Dropbox\\ArtInfo\\Quiz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\QuizzQuestions.xml");
			double number = (double)xDoc.XPathEvaluate("count(//quizz/"+ Alt + "/question[@number='" + questions[i] + "']/option)");

			return number;
		}

		public void loadResults()
		{
			win_Results win_Results = new win_Results();
			win_Results.lblResults.Content = lblCurrentScore.Content;
			this.Close();
			win_Results.Show();
			
		}

		
	}
}
